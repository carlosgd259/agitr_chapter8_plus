//this program toggles between rotation and translation
//commands,based on calls to a service.
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <agitr_chapter8_plus/Changerate.h>
//#include <agitr_chapter8_plus/changevel.h>

bool forward = true;
double newfrequency;
double newvelocity;
bool ratechanged = false;
bool startstop = true;
bool velchanged = false;
double vel;

bool toggleForward(
	std_srvs::Empty::Request &req,
	std_srvs::Empty::Response &resp){
        forward = !forward;
        ROS_INFO_STREAM("Now sending "<<(forward?
                "forward":"rotate")<< " commands.");
	return true;
}


bool togglestartstop(
	std_srvs::Empty::Request &req,
	std_srvs::Empty::Response &resp){
        startstop = !startstop;
        ROS_INFO_STREAM("Now The turtle is "<<(startstop?
                "Moving":"Stopped"));
	return true;
}



//bool changeRate(
//        agitr_chapter8_plus::Changerate::Request &req,
//        agitr_chapter8_plus::Changerate::Response &resp){

//        ROS_INFO_STREAM("Changing rate to "<<req.newrate);

//        newfrequency = req.newrate;
//        ratechanged = true;
//        return true;
//}

bool changeRate(
        agitr_chapter8_plus::Changerate::Request &req,
        agitr_chapter8_plus::Changerate::Response &resp){

        ROS_INFO_STREAM("Changing vel to "<<req.newrate);

        newvelocity = req.newrate;
        velchanged = true;
        return true;
}


int main(int argc, char **argv){
        ros::init(argc,argv,"pubvel_toggle_rate");
	ros::NodeHandle nh;
        
	ros::ServiceServer server = 
		nh.advertiseService("toggle_forward",&toggleForward);
                
        ros::ServiceServer server0 =
                nh.advertiseService("change_vel",&changeRate);
                
        ros::Publisher pub=nh.advertise<geometry_msgs::Twist>(
		"turtle1/cmd_vel",1000);

	ros::ServiceServer server1 = 
		nh.advertiseService("start_stop",&togglestartstop);


        ros::Rate rate(2);
	
	vel = 1.0;

	while(ros::ok()){
		

		if(startstop){
		    geometry_msgs::Twist msg;
              	    msg.linear.x = forward?vel:0.0;
                    msg.angular.z=forward?0.0:vel;
                    pub.publish(msg);
		}
		else{
		    geometry_msgs::Twist msg;
              	    msg.linear.x = 0.0;
                    msg.angular.z=0.0;
		    pub.publish(msg);
		}

		

		ros::spinOnce();

                if(ratechanged) {
                    rate = ros::Rate(newfrequency);
                    ratechanged = false;
                }

		if(velchanged) {
                    vel = newvelocity;
                    velchanged = false;
                }
		rate.sleep();
	}
}





